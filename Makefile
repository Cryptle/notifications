build:
	go build -o ns
run:
	./ns
tests:
	go test -v ./...
docker_build:
	docker build \
		-t ns:0.0.1 \
		-f docker/Dockerfile .
	docker tag ns:0.0.1 ns/latest
docker_run_all:
	docker-compose up