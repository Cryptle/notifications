package common

import (
	"os"
)

var (
	ListenHostPort     = os.Getenv("LISTEN_HOST_PORT")
	CORSHosts          = []string{"*"}
	NatsURL            = os.Getenv("NATS_SERVER")
	NatsBaseClient     = os.Getenv("BASE_CLIENT_ID")
	TwilioPhoneNumber  = os.Getenv("TWILIO_PHONE_NUMBER")
	SlackAPIToken      = os.Getenv("SLACK_API_TOKEN")
	EmailAddress       = os.Getenv("EMAIL_ADDRESS")
	EmailHost          = os.Getenv("EMAIL_HOST")
	EmailFrom          = os.Getenv("EMAIL_FROM")
	EmailUsername      = os.Getenv("EMAIL_USERNAME")
	EmailPassword      = os.Getenv("EMAIL_PASSWORD")
	DummyNotifications = os.Getenv("DUMMY_NOTIFICATIONS")
)

const (
	TopicNotifications = "nt.notifications"
	ClusterID          = "notification-cluster"
	NotifyTypeEmail    = "email"
	NotifyTypeSMS      = "sms"
	NotifyTypeSlack    = "slack"
)

// NotificationRequest struct to parse the incoming notification request
type NotificationRequest struct {
	Type string                 `json:"type" binding:"required,oneof=email sms slack"`
	Data map[string]interface{} `json:"data" binding:"required"`
}
