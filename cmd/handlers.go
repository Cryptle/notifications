package cmd

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	defs "gitlab.com/Cryptle/notifications/common"
	ntf "gitlab.com/Cryptle/notifications/notifications"
	"net/http"
)

// Notify endpoint sends a notification to NATS topic and returns a 200 status code if successful or an error otherwise
func Notify(c *gin.Context) {
	nats, ok := c.MustGet("nats").(*Nats)
	if !ok {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "cannot get nats connection"})
		return
	}

	var notificationRequest defs.NotificationRequest
	if err := c.ShouldBindJSON(&notificationRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	notification := &ntf.Notification{
		Type: notificationRequest.Type,
		Data: notificationRequest.Data,
	}
	ntfJson, err := json.Marshal(notification)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "error marshalling notification", "details": err.Error()})
		return
	}

	// publish the notification to NATS topic
	if err = nats.Conn().Publish(defs.TopicNotifications, ntfJson); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "error publishing notification", "details": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "notification sent"})
}
