package cmd

import (
	"fmt"
	"github.com/gin-gonic/gin"
	defs "gitlab.com/Cryptle/notifications/common"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// CORS policy
func CORS() gin.HandlerFunc {
	return func(c *gin.Context) {
		for _, loc := range defs.CORSHosts {
			c.Writer.Header().Set("Access-Control-Allow-Origin", loc)
		}
		c.Writer.Header().Set("Access-Control-Allow-Headers",
			"Content-Type, Content-Length, Accept-Encoding, Cache-Control")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}
		c.Next()
	}
}

// Server represents REST server
type Server struct {
	Router *gin.Engine
}

// New creates new REST server instance
func New() *Server {
	return &Server{}
}

// Start starts REST server
func (s *Server) Start(stop chan struct{}) error {
	var err error
	nats, err := NewNats(3)
	if err != nil {
		return err
	}
	if err = nats.NotificationsSubscribe(); err != nil {
		return err
	}

	s.Router = gin.New()
	s.Router.Use(gin.Recovery())
	s.Router.Use(CORS())

	s.Router.Use(func(c *gin.Context) {
		c.Set("nats", nats)
	})

	s.Router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "OK")
	})

	v1 := s.Router.Group("/api/v1")
	{
		v1.POST("/notify", Notify)
	}

	go func() {
		err = s.Router.Run(defs.ListenHostPort)
	}()

	fmt.Printf("start listening on %s\n", defs.ListenHostPort)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

loop:
	for {
		select {
		case <-c:
			fmt.Println("interrupted")
			break loop
		case <-stop:
			fmt.Println("stopped")
			break loop
		}
	}

	return err
}
