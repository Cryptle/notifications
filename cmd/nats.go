package cmd

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/nats-io/stan.go"
	defs "gitlab.com/Cryptle/notifications/common"
	nt "gitlab.com/Cryptle/notifications/notifications"
	"log"
)

// NewNats creates a new NATS connection
func NewNats(attempts int) (*Nats, error) {
	clientID := defs.NatsBaseClient + "-" + uuid.New().String()
	log.Printf("connecting to NATS server at %s clientID %s", defs.NatsURL, clientID)
	conn, err := stan.Connect(defs.ClusterID, clientID, stan.NatsURL(defs.NatsURL))
	if err != nil {
		return nil, err
	}

	return &Nats{
		conn:     conn,
		attempts: attempts,
	}, nil
}

type Nats struct {
	conn     stan.Conn
	sub      stan.Subscription
	attempts int
}

// Conn returns the NATS connection
func (n *Nats) Conn() stan.Conn {
	return n.conn
}

// Close closes the NATS connection
func (n *Nats) Close() error {
	return n.conn.Close()
}

// UnsubscribeNotifications unsubscribes from NATS topic
func (n *Nats) UnsubscribeNotifications() error {
	return n.sub.Unsubscribe()
}

// NotificationsSubscribe subscribes to NATS topic and sends notifications to the appropriate notifier
func (n *Nats) NotificationsSubscribe() (err error) {
	// subscribe to NATS topic using queue group
	n.sub, err = n.conn.QueueSubscribe(defs.TopicNotifications, "nt-queue-group", func(m *stan.Msg) {
		var notification nt.Notification
		if err = json.Unmarshal(m.Data, &notification); err != nil {
			log.Printf("failed to unmarshal notification: %v", err)
			return
		}

		// create notifier based on notification type
		var notifier nt.Notifier
		switch notification.Type {
		case defs.NotifyTypeEmail:
			notifier = nt.NewEmailNotifier(&nt.SmtpMailSender{})
		case defs.NotifyTypeSMS:
			notifier = nt.NewSMSNotifier(&nt.TheSMSSender{})
		case defs.NotifyTypeSlack:
			notifier = nt.NewSlackNotifier(&nt.TheSlackSender{})
		default:
			log.Printf("unknown notification type: %s", notification.Type)
			return
		}

		// if it fails do not acknowledge the message, it will be redelivered later
		if err = nt.SendNotification(notifier, &notification, n.attempts); err != nil {
			log.Printf("failed to send notification: %v", err)
			return
		}

		// acknowledge the message
		if err = m.Ack(); err != nil {
			log.Printf("failed to acknowledge message: %v", err)
			return
		}

	}, stan.DeliverAllAvailable(), stan.MaxInflight(1), stan.SetManualAckMode())

	return err
}
