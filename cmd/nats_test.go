package cmd_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/Cryptle/notifications/cmd"
	defs "gitlab.com/Cryptle/notifications/common"
	"testing"
)

func TestNatsConnAndSub(t *testing.T) {
	defs.NatsURL = "nats://localhost:4222"
	defs.NatsBaseClient = "client"
	nats, err := cmd.NewNats(3)
	assert.NoError(t, err)
	assert.NotEmpty(t, nats)
	assert.NotEmpty(t, nats.Conn())
	assert.NoError(t, nats.NotificationsSubscribe())
	assert.NoError(t, nats.UnsubscribeNotifications())
	assert.NoError(t, nats.Close())
}
