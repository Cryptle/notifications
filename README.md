# Notification System

The Notification System is a scalable system for sending notifications via multiple channels. 
It provides an API for accepting notification requests and uses 
NATS for reliable message delivery and Nginx for load balancing. 

## Features

- Send notifications via email, SMS, and Slack
- Horizontally scalable, allowing for increased load handling
- At least once message delivery guarantee ensuring no message is lost in the process

## Getting Started

### Prerequisites

- [Go](https://golang.org/) 1.20.2 or higher
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

### Installation

1. Clone the repository:
    ```bash
    git clone git@gitlab.com:Cryptle/notifications.git
    cd notifications
    ```
2. Build the application:
    ```bash
    make build
    ```
## Configuration
The application can be configured using environment variables. The following variables:
- `LISTEN_HOST_PORT`: The host and port on which the application listens (default: 0.0.0.0:7020).
- `NATS_SERVER`: The NATS server URL (default: nats://nats:4222).
- `NATS_CLUSTER_ID`: The NATS cluster ID (default: notification-cluster).
- `BASE_CLIENT_ID`: The base client ID used for generating unique client IDs (default: client).
- `TWILIO_PHONE_NUMBER`: Twilio phone number for sending SMS notifications.
- `SLACK_API_TOKEN`: Slack API token for sending notifications to Slack.
- `EMAIL_ADDRESS`: Email address for sending email notifications.
- `EMAIL_HOST`: Email server hostname.
- `EMAIL_FROM`: Email sender address.
- `EMAIL_USERNAME`: Email account username.
- `EMAIL_PASSWORD`: Email account password.
- `DUMMY_NOTIFICATIONS=true`: Send dummy notifications.

`Note:` you need to set `DUMMY_NOTIFICATIONS=false` and other environment variables for sending real notifications.

## Docker
The Notification System is dockerized

### Building the Docker Image

- Make sure Docker is installed and running on your system.
- Run the following command in your terminal:

```bash
make docker_build
```
This will build the Docker image

## Scaling

The Notification System can be scaled horizontally
For example, to start 3 instances of the Notification System, use the following command:

```bash
docker-compose up --scale ns=3
```
This will start 3 instances of the Notification System with a load balancer.
Now you can send POST request on `http://localhost:7020/api/v1/notify` endpoint.

## Examples

### Sending a Slack Notification

```json
  {
    "type": "slack",
    "data": {
        "channel": "test-channel",
        "text": "slack notification example."
    }
  }
```
Replace `test-channel` with your Slack channel name

### Sending an Email Notification

Send a notification to an email address:

```json
  {
    "type": "email",
    "data": {
        "to": "exmple@example.com",
        "subject": "Test subject",
        "body": "email notification example."
    }
  }
```

Replace `exmple@example.com` with the actual recipient email address.

### Sending an SMS Notification

Send an SMS notification using Twilio:
```json
  {
    "type": "sms",
    "data": {
        "to": "+1234567890",
        "body": "SMS notification example"
    }
  }
```
Replace `+1234567890` with the actual recipient phone number.

When execute some of the above examples, you will see something like this:
![Screenshot of the application](https://gitlab.com/Cryptle/notifications/-/raw/main/assets/example.png)

## Testing

You can run tests using the following command:

```bash
make tests
```
