package notifications

import (
	"encoding/json"
	"fmt"
	"log"
	"time"
)

type Notification struct {
	Data map[string]interface{} `json:"data"`
	Type string                 `json:"type"`
}

type Notifier interface {
	Notify(n *Notification) error
}

// SendNotification sends a notification with a given notifier and attempts to send it a given number of times
func SendNotification(n Notifier, notification *Notification, attempts int) error {
	for i := 0; i < attempts; i++ {
		if err := n.Notify(notification); err == nil {
			return nil
		}
		time.Sleep(1 * time.Second)
	}
	return fmt.Errorf("failed to send notification after %d attempts", attempts)
}

// dataToStruct helper function to convert a map to a struct
func dataToStruct(data map[string]any, res any) error {
	dataBytes, err := json.Marshal(data)
	if err != nil {
		log.Printf("failed to marshal data: %v", err)
		return err
	}
	if err = json.Unmarshal(dataBytes, res); err != nil {
		log.Printf("failed to unmarshal data: %v", err)
		return err
	}
	return nil
}
