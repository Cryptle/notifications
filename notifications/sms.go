package notifications

import (
	"github.com/twilio/twilio-go"
	openapi "github.com/twilio/twilio-go/rest/api/v2010"
	defs "gitlab.com/Cryptle/notifications/common"
	"log"
)

type SMSSender interface {
	SendMessage(to string, body string) error
}

type TheSMSSender struct{}

// SendMessage sends an SMS message
func (s *TheSMSSender) SendMessage(to string, body string) error {
	if defs.DummyNotifications == "true" {
		log.Printf("dummy SMS notification sent to %s, body %s", to, body)
		return nil
	}
	client := twilio.NewRestClient()
	params := &openapi.CreateMessageParams{}
	params.SetTo(to)
	params.SetFrom(defs.TwilioPhoneNumber)
	params.SetBody(body)

	// send the message
	if _, err := client.Api.CreateMessage(params); err != nil {
		log.Printf("failed to send SMS: %v", err)
		return err
	}
	return nil
}

type SMSNotifier struct {
	sms SMSSender
}

type SMSNotification struct {
	To   string `json:"to"`
	Body string `json:"body"`
}

// NewSMSNotifier creates a new SMS notifier
func NewSMSNotifier(sms SMSSender) *SMSNotifier {
	return &SMSNotifier{sms: sms}
}

// Notify call the SMS sender
func (sn *SMSNotifier) Notify(n *Notification) error {
	var smsNotification SMSNotification
	if err := dataToStruct(n.Data, &smsNotification); err != nil {
		return err
	}

	if err := sn.sms.SendMessage(smsNotification.To, smsNotification.Body); err != nil {
		return err
	}
	return nil
}
