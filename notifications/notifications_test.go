package notifications

import (
	"github.com/stretchr/testify/assert"
	defs "gitlab.com/Cryptle/notifications/common"
	"testing"
)

type MockMailSender struct {
	t *testing.T
}

func (m *MockMailSender) SendMail(to, subject, body string) error {
	assert.Equal(m.t, "test@example.com", to)
	assert.Equal(m.t, "Test Subject", subject)
	assert.Equal(m.t, "Test Body", body)
	return nil
}

type MockSlackSender struct {
	t *testing.T
}

func (ms *MockSlackSender) SendMessage(channel string, text string) error {
	assert.Equal(ms.t, channel, "test.channel")
	assert.Equal(ms.t, text, "Test Text")
	return nil
}

type MockSMSSender struct {
	t *testing.T
}

func (sms *MockSMSSender) SendMessage(to string, body string) error {
	assert.Equal(sms.t, to, "1234567890")
	assert.Equal(sms.t, body, "Test")
	return nil
}

func TestEmailNotifier_Notify(t *testing.T) {
	notifier := NewEmailNotifier(&MockMailSender{t: t})
	notification := &Notification{
		Type: defs.NotifyTypeEmail,
		Data: map[string]interface{}{
			"to":      "test@example.com",
			"subject": "Test Subject",
			"body":    "Test Body",
		},
	}
	assert.NoError(t, notifier.Notify(notification))
}

func TestSlackNotifier_Notify(t *testing.T) {
	notifier := NewSlackNotifier(&MockSlackSender{t: t})
	notification := &Notification{
		Type: defs.NotifyTypeSlack,
		Data: map[string]interface{}{
			"channel": "test.channel",
			"text":    "Test Text",
		},
	}
	assert.NoError(t, notifier.Notify(notification))
}

func TestSMSNotifier_Notify(t *testing.T) {
	notifier := NewSMSNotifier(&MockSMSSender{t: t})
	notification := &Notification{
		Type: defs.NotifyTypeSMS,
		Data: map[string]interface{}{
			"to":   "1234567890",
			"body": "Test",
		},
	}
	assert.NoError(t, notifier.Notify(notification))
}
