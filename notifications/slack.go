package notifications

import (
	"github.com/slack-go/slack"
	defs "gitlab.com/Cryptle/notifications/common"
	"log"
)

type SlackSender interface {
	SendMessage(channel string, text string) error
}

type TheSlackSender struct{}

// SendMessage sends a Slack message
func (s *TheSlackSender) SendMessage(channel string, text string) error {
	if defs.DummyNotifications == "true" {
		log.Printf("dummy slack notification sent channel %s, text %s", channel, text)
		return nil
	}
	api := slack.New(defs.SlackAPIToken)
	channelID, timestamp, err := api.PostMessage(
		channel,
		slack.MsgOptionText(text, false),
	)
	if err != nil {
		return err
	}
	log.Printf("message successfully sent to channel %s at %s", channelID, timestamp)
	return nil
}

type SlackNotifier struct {
	slack SlackSender
}

type SlackNotification struct {
	Channel string `json:"channel"`
	Text    string `json:"text"`
}

// NewSlackNotifier creates a new Slack notifier
func NewSlackNotifier(s SlackSender) *SlackNotifier {
	return &SlackNotifier{slack: s}
}

// Notify call the Slack sender
func (slk *SlackNotifier) Notify(n *Notification) error {
	var slackNotification SlackNotification
	if err := dataToStruct(n.Data, &slackNotification); err != nil {
		return err
	}
	if err := slk.slack.SendMessage(slackNotification.Channel, slackNotification.Text); err != nil {
		return err
	}
	return nil
}
