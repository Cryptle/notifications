package notifications

import (
	defs "gitlab.com/Cryptle/notifications/common"
	"log"
	"net/smtp"
)

type SmtpMailSender struct{}

// SendMail sends an email
func (s *SmtpMailSender) SendMail(to, subject, body string) error {
	if defs.DummyNotifications == "true" {
		log.Printf("dummy email notification sent to %s, subject %s body %s", to, subject, body)
		return nil
	}
	auth := smtp.PlainAuth("", defs.EmailUsername, defs.EmailPassword, defs.EmailHost)
	_to := []string{to}
	msg := []byte("To: " + to + "\r\n" +
		"Subject: " + subject + "\r\n" +
		"\r\n" +
		body + "\r\n")
	return smtp.SendMail(defs.EmailAddress, auth, defs.EmailFrom, _to, msg)
}

type MailSender interface {
	SendMail(to, subject, body string) error
}

type EmailNotifier struct {
	mailer MailSender
}

type EmailNotification struct {
	To      string
	Subject string
	Body    string
}

// NewEmailNotifier creates a new email notifier
func NewEmailNotifier(m MailSender) *EmailNotifier {
	return &EmailNotifier{mailer: m}
}

// Notify calls the mailer to send an email
func (e *EmailNotifier) Notify(n *Notification) error {
	var emailNotification EmailNotification
	if err := dataToStruct(n.Data, &emailNotification); err != nil {
		return err
	}
	err := e.mailer.SendMail(emailNotification.To, emailNotification.Subject, emailNotification.Body)
	if err != nil {
		log.Printf("failed to send email: %v", err)
		return err
	}
	return nil
}
