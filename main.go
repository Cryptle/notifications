package main

import (
	"fmt"
	"gitlab.com/Cryptle/notifications/cmd"
)

func main() {
	if err := cmd.New().Start(nil); err != nil {
		fmt.Println(err)
	}
}
